import os
from flask import Flask,render_template,request,redirect,url_for,flash
from flask_wtf import FlaskForm,RecaptchaField
from wtforms import StringField,PasswordField
from wtforms.validators import InputRequired,Email,Length,EqualTo

app=Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

# CONFIGURATIONS
app.config['SECRET_KEY']='6LcVYeUUAAAAAHs_K9SE0EYcv3Cq9USAV7WgMbTb'
app.config['RECAPTCHA_PRIVATE_KEY']= os.environ.get('RECAPTCHA_PRIVATE_KEY')
app.config['RECAPTCHA_PUBLIC KEY']= os.environ.get('RECAPTCHA_PUBLIC KEY')

#LOGINFORM
class LoginForm(FlaskForm):
    username=StringField('Username',validators=[InputRequired(),Length(min=8,max=30,message='username is too long')])
    password=PasswordField('Password',validators=[InputRequired(),Length(min=8,max=50,message='username is too long')])
    recaptcha=RecaptchaField()
#REGISTRATIONFORM
class RegistrationForm(FlaskForm):
    firstname=StringField('Firstname',validators=[InputRequired(),Length(min=8,max=30,message='username is too long')])
    lastname=StringField('Lastname',validators=[InputRequired(),Length(min=8,max=30,message='username is too long')])
    username=StringField('Username',validators=[InputRequired(),Length(min=8,max=30,message='username is too long')])
    email=StringField('Email',validators=[InputRequired(),Length(min=12,max=100),Email()])
    password=PasswordField('Password',validators=[InputRequired(),Length(min=8,max=150),EqualTo('confirm')])
    confirm=PasswordField('Confirm password',validators=[InputRequired()])

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/dash')
def dash():
    return render_template('dash.html')
@app.route('/register',methods=['GET','POST'])
def register():
    form=RegistrationForm()
    if request.method=='POST':
        if form.validate_on_submit():
            flash('Account created succesfully','success')
            return redirect(url_for('login'))
        flash('Account cannot be created','danger')
        return redirect(url_for('register'))
    return render_template('register.html',form=form)
@app.route('/login',methods=['GET','POST'])
def login():
    form=LoginForm()
    if request.method=='POST':
        if form.validate_on_submit:
            username=form.username.data
            password=form.password.data
            return  render_template('login.html',form=form)
    return render_template('login.html',form=form)

if __name__=='__main__':
    app.run(debug=True)